package mx.com.allianz.util.service.rest.mail;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import mx.com.allianz.util.service.mail.api.MailService;
import mx.com.allianz.util.service.mail.api.exception.AllianzMailServiceException;

@ApplicationPath("/mail")
/*
 * @Component sirve para garantizar la seguridad del path
 */
@Component(
	immediate = true, property = {"jaxrs.application=true"},
	service = Application.class
)
public class MailServiceRest extends Application {
	/**
	 * M\u00e9todo que retorna una colecci\u00f3n.
	 * 
	 * @return Collections.singleton((Object)this)
	 */
	@Override
	public Set<Object> getSingletons() {
		return Collections.singleton((Object) this);
	}

	/**
	 * Operaci\u00f3n de servicio rest generico para envio de correos
	 * 
	 * @return void
	 */
	// @PUT es el nombre del procesamiento de la petici\u00f3n.
	@PUT
	// @Path sirve para poner la ruta relativa donde la clase ser\u00e1
	// hospedada.
	@Path("/sendMail")
	public void sendMail(
			// El valor @DefaultValue se utiliz\u00e1 para definir el valor por
			// defecto para el par\u00e1metro matrixParam.
			// @QueryParam se utiliz\u00e1 para extraer los par\u00e1metros de
			// consulta del "componente de consulta" de la URL de la solicitud.
			@DefaultValue("testinfzoneallianz@gmail.com") @QueryParam("from") String from,
			@DefaultValue("alfonso.marquez@testingzone.com.mx") @QueryParam("to") String to,
			@DefaultValue("Prueba Servicio de Envio de Correos") @QueryParam("subject") String subject,
			@DefaultValue("Cuerpo de prueba Servicio de Envio de Correos.") @QueryParam("body") String body,
			@QueryParam("ccs") String[] ccArray, @QueryParam(value="bccs") String[] bccArray) 
					throws AllianzMailServiceException {
		_log.debug("Entrando a MailServiceRest -> sendMail");
		_log.debug("from = " + from + ", to = " + to + ", subject = " + subject + ", body = " + body);
		_log.debug("ccArray = " + ccArray + ", bccArray = " + bccArray);

		try {
			mailService.sendMail(from, to, subject, body, Optional.ofNullable(ccArray), Optional.ofNullable(bccArray));
		}catch (Exception e) {
			e.printStackTrace();
		}
//		return "ok";
	}

	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL)
	private void setMailService(MailService mailService) {
		this.mailService = mailService;
	}

	protected void unsetMailService(MailService mailService) {
		this.mailService = null;
	}

	private MailService mailService;
	private static Log _log = LogFactoryUtil.getLog(MailServiceRest.class);
}
