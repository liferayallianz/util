package mx.com.allianz.util.service.mail.api;

import java.util.Optional;

import aQute.bnd.annotation.ProviderType;
import mx.com.allianz.util.service.mail.api.exception.AllianzMailServiceException;

@ProviderType
public interface MailService {

	void sendMail(String from, String to, String subject, String body) throws AllianzMailServiceException;

	void sendMail(String from, String to, String subject, String body, String cc, String bcc)
			throws AllianzMailServiceException;

	void sendMail(String from, String to, String subject, String body, boolean cType, String[] cArray)
			throws AllianzMailServiceException;

	void sendMail(String from, String to, String subject, String body, Optional<String[]> ccArray,
			Optional<String[]> bccArray) throws AllianzMailServiceException;
}