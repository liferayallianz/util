package mx.com.allianz.util.service.mail.api.exception;

public class AllianzMailServiceException extends Exception {

	private static final long serialVersionUID = 4861274878032949649L;

	public AllianzMailServiceException() {
		super();
	}

	public AllianzMailServiceException(String message) {
		super(message);
	}

	public AllianzMailServiceException(Throwable cause) {
        super(cause);
    }
}
