package mx.com.allianz.util.service.mail.service.configuration;

import aQute.bnd.annotation.metatype.Meta;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

/**
 * @author Testing Zone
 */
@ExtendedObjectClassDefinition(category = "Productivity")
@Meta.OCD(
		id = "mx.com.allianz.util.service.mail.service.configuration.MailServiceConfiguration",
		localization = "content/Language",
		name = "mx.com.allianz.util.service.mail.service.configuration.name"
)
public interface MailServiceConfiguration {

	@Meta.AD(deflt = "testinfzoneallianz@gmail.com", required = false)
	public String mailSenderUser();
	
	@Meta.AD(deflt = "T3$T!NGZ0N3", required = false)
	public String mailSenderPassword();
	
	@Meta.AD(deflt = "smtp.gmail.com", required = false)
	public String mailSmtpHost();
	
	@Meta.AD(deflt = "465", required = false)
	public int mailSmtpSocketFactoryPort();

	@Meta.AD(deflt = "javax.net.ssl.SSLSocketFactory", required = false)
	public String mailSmtpSocketFactoryClass();

	@Meta.AD(deflt = "true", required = false)
	public boolean mailSmtpAuth();

	@Meta.AD(deflt = "465", required = false)
	public int mailSmtpPort();

}