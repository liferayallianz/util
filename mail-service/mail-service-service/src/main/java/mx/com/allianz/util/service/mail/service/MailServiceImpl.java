package mx.com.allianz.util.service.mail.service;


import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import javax.activation.CommandMap;
import javax.activation.MailcapCommandMap;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import aQute.bnd.annotation.metatype.Configurable;
import mx.com.allianz.util.service.mail.api.MailService;
import mx.com.allianz.util.service.mail.api.exception.AllianzMailServiceException;
import mx.com.allianz.util.service.mail.service.configuration.MailServiceConfiguration;

@Component(
	configurationPid="mx.com.allianz.util.service.mail.service.configuration.MailServiceConfiguration",
	immediate = true,
	property = {
	},
	service = MailService.class
)
public class MailServiceImpl implements MailService {

//	Liferay Mail
//	@Override
//	public void sendMail(String from, String to, String subject, String body) throws AllianzMailServiceException {
//		MailMessage mailMessage=new MailMessage();
//        try {
//        		from = stringEncode(from);
//			mailMessage.setFrom(new InternetAddress(from));
//			mailMessage.setTo(new InternetAddress(Optional.ofNullable(to).orElseThrow(AllianzMailServiceException::new).trim()));
//		} catch (AddressException e) {
//			e.printStackTrace();
//		}
//
//        mailMessage.setSubject(stringEncode(subject));
//        mailMessage.setBody(body);
//        mailMessage.setHTMLFormat(true);
//        MailServiceUtil.sendEmail(mailMessage);
//	}
	
	@Override
	public void sendMail(String from, String to, String subject, String body) throws AllianzMailServiceException {
		sendMail(from,to,subject,body,Optional.empty(),Optional.empty());
	}
	
	@Override
	public void sendMail(String from, String to, String subject, String body, String cc, String bcc) throws AllianzMailServiceException {
		sendMail(from,to,subject,body,Optional.of(new String[]{cc}),Optional.of(new String[]{cc}));
	}
	
	@Override
	public void sendMail(String from, String to, String subject, String body, boolean cType, String... cArray) throws AllianzMailServiceException {
		sendMail(from,to,subject,body,(cType?Optional.ofNullable(cArray):null),(cType?null:Optional.ofNullable(cArray)));
	}
	
	
	@Override
	public void sendMail(String from, String to, String subject, String body, Optional<String[]> ccArray, Optional<String[]> bccArray) throws AllianzMailServiceException {
		if (_log.isDebugEnabled()) {
			_log.debug("Ingresando a MailService -> sendMail"); 
			_log.debug("sendMail, from = " + from); 
			_log.debug("sendMail, to = " + to); 
			_log.debug("sendMail, subject = " + subject); 
			_log.debug("sendMail, body = " + body); 

			_log.debug("Configured Mail Service Properties : {" 
			+ "  mailSenderUser:" + getMailSenderUser()
			+ ", mailSenderPassword:" + getMailSenderPassword()
			+ ", mailSmtpHost:" + getMailSmtpHost()
			+ ", mailSmtpSocketFactoryPort:" + getMailSmtpSocketFactoryPort() 
			+ ", mailSmtpSocketFactoryClass:" + getMailSmtpSocketFactoryClass()
			+ ", mailSmtpAuth:" + getMailSmtpAuth() + ", mailSmtpPort:" + getMailSmtpPort() + " }");
		}
		
		Properties props = new Properties();
		props.put("mail.smtp.host", getMailSmtpHost());
		props.put("mail.smtp.socketFactory.port", String.valueOf(getMailSmtpSocketFactoryPort()));
		props.put("mail.smtp.socketFactory.class", getMailSmtpSocketFactoryClass());
		props.put("mail.smtp.auth", String.valueOf(getMailSmtpAuth()));
		props.put("mail.smtp.port", String.valueOf(getMailSmtpPort()));
		
		MailcapCommandMap mc = (MailcapCommandMap) CommandMap.getDefaultCommandMap(); 
		mc.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html"); 
		mc.addMailcap("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml"); 
		mc.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain"); 
		mc.addMailcap("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed"); 
		mc.addMailcap("message/rfc822;; x-java-content- handler=com.sun.mail.handlers.message_rfc822");
		
		Session session = Session.getInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(getMailSenderUser(),
							getMailSenderPassword());
				}
			});

		try {
			
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			addCopies(Optional.of(new String[]{to}), Message.RecipientType.TO, message);
			message.setSubject(stringEncode(subject));
			addCopies(ccArray, Message.RecipientType.CC, message);
			if ( bccArray != null && bccArray.isPresent() )
				addCopies(bccArray, Message.RecipientType.BCC, message);

			message.setContent( body, "text/html; charset=ISO-8859-1" );
//			message.setContent(body, "text/html; charset=ISO-8859-1");
		    message.saveChanges();

			Transport.send(message);
		} catch (Exception e) {
			if (_log.isErrorEnabled()){
				_log.error(e.getMessage());
			}
			e.printStackTrace();
		}
	}
	
	private String stringEncode(String cadena) {
//		return new String(cadena.getBytes(),Charset.forName("ISO-8859-1"));
		return new String(cadena.getBytes(),Charset.forName("UTF-8"));
	}
	
	private Optional<InternetAddress[]> getInternetAddresses(String mails) {
		Optional<InternetAddress[]> internetAddress = Optional.empty();
		if (mails != null){
			try {
				internetAddress = Optional.ofNullable(InternetAddress.parse(mails));
			} catch (AddressException e) {
				e.printStackTrace();
			}
		};
		return internetAddress;
	}

	private void addCopies(Optional<String[]> mailArray, Message.RecipientType type, Message message) {
		mailArray.map(Arrays::asList).map(list -> String.join(",", list)).
		flatMap(mails -> getInternetAddresses(mails)).ifPresent(internetAddresses -> { try {
			message.addRecipients(type,internetAddresses);
		} catch (MessagingException e) { e.printStackTrace(); }
		});
	}
	
	public String getMailSenderUser(){
		return _configuration.mailSenderUser();
	}
	
	public String getMailSenderPassword(){
		return _configuration.mailSenderPassword();
	}
	
	public String getMailSmtpHost(){
		return _configuration.mailSmtpHost();
	}
	
	public String getMailSmtpSocketFactoryPort(){
		return String.valueOf(_configuration.mailSmtpSocketFactoryPort());
	}
	
	public String getMailSmtpSocketFactoryClass(){
		return _configuration.mailSmtpSocketFactoryClass();
	}
	
	public String getMailSmtpAuth(){
		return String.valueOf(_configuration.mailSmtpAuth());
	}
	
	public String getMailSmtpPort(){
		return String.valueOf(_configuration.mailSmtpPort());
	}
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		_configuration = Configurable.createConfigurable(
				MailServiceConfiguration.class, properties);
		if (_log.isInfoEnabled()) {
			_log.info("Configured Mail Service Properties : {" 
			+ "  mailSenderUser:" + getMailSenderUser()
			+ ", mailSenderPassword:" + getMailSenderPassword()
			+ ", mailSmtpHost:" + getMailSmtpHost()
			+ ", mailSmtpSocketFactoryPort:" + getMailSmtpSocketFactoryPort() 
			+ ", mailSmtpSocketFactoryClass:" + getMailSmtpSocketFactoryClass()
			+ ", mailSmtpAuth:" + getMailSmtpAuth() + ", mailSmtpPort:" + getMailSmtpPort() + " }");
		}
	}
	
	private static Log _log = LogFactoryUtil.getLog(MailServiceImpl.class);
	private volatile MailServiceConfiguration _configuration;
}