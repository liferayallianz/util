package mx.com.allianz.captcha.custom.portlet;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.captcha.CaptchaTextException;
import com.liferay.portal.kernel.captcha.CaptchaUtil;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;

@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		// Se agregan parametros para llamado de eventos de portlet
        "com.liferay.portlet.requires-namespaced-parameters=false",
        "com.liferay.portlet.ajaxable=true",
		// Fin parametros para llamado de eventos de portlet
		// Se agregan parametros para compartir atributos de request en la session
        "com.liferay.portlet.private-request-attributes=false",
        "com.liferay.portlet.private-session-attributes=false",
        "com.liferay.portlet.remoteable=true",
        "com.liferay.portlet.preferences-unique-per-layout=true",
		// Fin parametros para compartir atributos de request en la session
		"javax.portlet.display-name=Util Captcha",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.supported-processing-event=solicitanteTramiteClienteEvent;http://mx-allianz-liferay-namespace.com/events/solicitanteTramiteClienteEvent",
        "javax.portlet.supported-processing-event=detalleQuejaEvent;http://mx-allianz-liferay-namespace.com/events/detalleQuejaEvent",
		"javax.portlet.security-role-ref=power-user,user,guest"
	},
	service = Portlet.class
)
public class CustomCaptchaPortlet extends MVCPortlet {
	
	public static final String CLIENTE_TRAMITE_EVENT = "solicitanteTramiteClienteEvent";
	public static final String CLIENTE_QUEJA_EVENT = "detalleQuejaEvent";
	public static final String EVENTO_SIN_NOMBRE = "EVENTO SIN NOMBRE";
	
	public static final String LLAVE_CLIENTE_HABILITADO = "clienteHabilitado"; 

	@Override
	public void processAction(ActionRequest actionRequest, ActionResponse actionResponse)
			throws IOException, PortletException {
  		testCaptcha(actionRequest);

    }
	
	private String getCaptchaValueFromSession(PortletSession session) {
	    Enumeration<String> atNames = session.getAttributeNames();
	    while (atNames.hasMoreElements()) {
	        String name = atNames.nextElement();
	        if (name.contains("CAPTCHA_TEXT")) {
	            return (String) session.getAttribute(name);
	        }
	    }
	    return null;
	}
	
	  @Override
	  public void serveResource(ResourceRequest resourceRequest,
	              ResourceResponse resourceResponse) throws  IOException, PortletException {
		  JSONObject jsonMessageObject = JSONFactoryUtil.createJSONObject();
		  try {
			  String parametro = ParamUtil.getString(resourceRequest, "captchaActionType");
			  if(parametro !=null && "testCaptchaURL".equals((String)parametro)){
				  Boolean testCaptcha = testCaptcha(resourceRequest);
				  System.out.println("captcha return testCaptcha = " + testCaptcha);
				  jsonMessageObject.put("success", testCaptcha);
				  System.out.println("jsonMessageObject = " + jsonMessageObject.toJSONString());
				  resourceResponse.setContentType("text/javascript");
				  resourceResponse.getWriter().write(jsonMessageObject.toJSONString());
			  }else {
			  		CaptchaUtil.serveImage(resourceRequest, resourceResponse);
			  }
	        } catch (Exception e) {
	        		System.out.println("no deberia llegar hasta aca pops");
	            e.printStackTrace();
	        }
	  }

	  private boolean testCaptcha(PortletRequest request){
		  System.out.println("captcha action");
		  boolean validCaptcha = true;
		  try{
		    		PortletSession session = request.getPortletSession();
		    		String gRecaptchaRequest = ParamUtil.getString(request, "gRecaptchaResponse");
		    		System.out.println("gRecaptchaRequest = " + gRecaptchaRequest);
		    		request.getParameterMap().forEach((val,key) -> System.out.println(val + " = " + key +  ", " ));
		    		
		    		if (gRecaptchaRequest != null && !gRecaptchaRequest.isEmpty()) {
			    		boolean verify = VerifyReCaptcha.verify(gRecaptchaRequest);
			    		System.out.println(verify);
			    		if (!verify) {
			    			throw new CaptchaTextException("Error en el Captcha! Por favor vuelva a intentar.");
			    		} 
		    		} else {
		    			String captchaTextSession = getCaptchaValueFromSession(session);
		    			String enteredCaptchaText = ParamUtil.getString(request, "captchaTextValue");
			    		System.out.println("captcha captchaTextSession " + captchaTextSession);
			    		System.out.println("captcha enteredCaptchaText " + enteredCaptchaText);

		    			if (Validator.isNull(captchaTextSession) || Validator.isNull(enteredCaptchaText) ) {
			    			throw new CaptchaTextException("Error interno! Por favor vuelva a intentar.");
			    		} else if (!enteredCaptchaText.equals(captchaTextSession)) {
			    			throw new CaptchaTextException("Texto de captcha invalido. Por favor vuelva a intentar.");
			    		}
		    		}
			}catch(CaptchaTextException | IOException e) {
				System.out.println("Deberia pintar aqui");
				SessionErrors.add(request, e.getClass(), e);
				e.printStackTrace();
				System.out.println("y seguir el flujo");
				validCaptcha = false;
			}
		    return validCaptcha;
	  }
	  public void render(RenderRequest renderRequest, RenderResponse renderResponse)
				throws IOException, PortletException {
			try {
				Optional<PortletSession> sessionO = Optional.ofNullable(renderRequest)
															.map(RenderRequest::getPortletSession);
				boolean clienteHabilitado = 
						sessionO.map(session -> 
										session.getAttribute(LLAVE_CLIENTE_HABILITADO,PortletSession.APPLICATION_SCOPE)
								)
								.filter(p -> p instanceof Boolean)
								.map(p -> (Boolean) p)
								.orElse(false);
				_log.info("CustomCaptchaPortlet -> render -> clienteHabilitado, sube = " + clienteHabilitado);
				renderRequest.setAttribute(LLAVE_CLIENTE_HABILITADO, clienteHabilitado);
			}catch(Exception e){
				e.printStackTrace();

			}
	  }
	  
		@Override
		public void processEvent(EventRequest request, EventResponse response) throws PortletException, IOException {
			_log.info("CustomCaptchaPortlet -> processEvent -> Entrando");

			//Gson gson = new Gson();
			PortletSession session = request.getPortletSession();
			Optional<Event> event = Optional.ofNullable(request).map(EventRequest::getEvent);
			_log.info("CustomCaptchaPortlet -> processEvent -> procesando el evento" + event.map(Event::getName).get());
			
			switch(event.map(Event::getName).orElse(EVENTO_SIN_NOMBRE)){
				case CLIENTE_TRAMITE_EVENT:
					event.map(Event::getValue)
						 .filter(val -> (val instanceof String))
						 .map(val -> (String) val)
						 .ifPresent(obj -> {
							_log.info("CustomCaptchaPortlet -> processEvent -> tramite = " + obj);
							_log.info("CustomCaptchaPortlet -> processEvent -> tramite cliente habilitado = " + obj.contains("\"tramiteClienteHabilitado\":true"));
							session.setAttribute(LLAVE_CLIENTE_HABILITADO, obj.contains("\"tramiteClienteHabilitado\":true"),
										PortletSession.APPLICATION_SCOPE);
						 });
				break;
				case CLIENTE_QUEJA_EVENT:
					event.map(Event::getValue)
						 .filter(val -> (val instanceof String))
						 .map(json -> (String) json)
						 .ifPresent(obj -> {
							_log.info("CustomCaptchaPortlet -> processEvent -> queja = " + obj);
							_log.info("CustomCaptchaPortlet -> processEvent -> queja cliente habilitado = " + obj.contains("\"quejaClienteHabilitado\":true"));
							session.setAttribute(LLAVE_CLIENTE_HABILITADO, obj.contains("\"quejaClienteHabilitado\":true"),
										PortletSession.APPLICATION_SCOPE);
						 });
				break;
				default:
			}
			super.processEvent(request, response);
		}
	  
      protected boolean isCheckMethodOnProcessAction() {
            return _CHECK_METHOD_ON_PROCESS_ACTION;
      }
      
      private static final boolean _CHECK_METHOD_ON_PROCESS_ACTION = false;
      private static Log _log = LogFactoryUtil.getLog(CustomCaptchaPortlet.class);

}