<%@ include file="/init.jsp" %>

<%@page import="com.liferay.portal.kernel.captcha.CaptchaTextException"%>
<%@page import="com.liferay.portal.kernel.captcha.CaptchaMaxChallengesException"%>



<div >
<liferay-ui:error exception="<%= CaptchaTextException.class %>" message="text-verification-failed" />
<portlet:actionURL  var="testCaptchaActionURL" name="processAction"/>
<portlet:resourceURL var="captchaURL" id="captchaURL">
    <portlet:param name="captchaActionType" value="loadCaptchaURLImage"/>
</portlet:resourceURL>
<portlet:resourceURL var="testCaptchaURL" id="testCaptchaURL">
    <portlet:param name="captchaActionType" value="testCaptchaURL"/>
</portlet:resourceURL>
<aui:form name="fm_captcha" method="post"  action="<%=testCaptchaActionURL.toString()%>">
	<liferay-ui:captcha url="<%=captchaURL.toString()%>" />
</aui:form>
</div>


<aui:script use="aui-base,aui-io-request,liferay-form,aui-form-validator">
console.log("captcha v1.1");
var tramiteClienteHabilitadoJS = false;
console.log("captcha tramiteClienteHabilitadoJS = " + tramiteClienteHabilitadoJS);


Liferay.on('validaCaptcha', function (event) {
	var eventoRespuesta = {};
	if(tramiteClienteHabilitadoJS){
		 eventoRespuesta.valido = true;
		 eventoRespuesta.catpcha = 'NA';
	} else {
		var formValidator = Liferay.Form.get('<portlet:namespace />fm_captcha').formValidator;
		formValidator.validate();
		var captcha = A.one('#<portlet:namespace />captchaText') ;
		var captchaTextValue = captcha ? captcha.get('value'): 'NA';
		eventoRespuesta.valido = !formValidator.hasErrors();//('NA' != captchaTextValue) ? (!formValidator.hasErrors()) : true;
		console.log('validaCaptcha eventoRespuesta.valido = ' + eventoRespuesta.valido);
		console.log('validaCaptcha captchaTextValue = ' + captchaTextValue);
		console.log('(NA != captchaTextValue) = ' + ('NA' != captchaTextValue));

		if('NA' != captchaTextValue) {
			//eventoRespuesta.catpcha = '2345';
			//Liferay.fire('validaCaptchaRespuesta', eventoRespuesta );
			if(eventoRespuesta.valido){
				console.log('validaCaptcha captcha = ' + captcha);

				eventoRespuesta.catpcha = ('NA' != captchaTextValue) ? captchaTextValue : responseReCaptcha;
				console.log('captchaTextValue captcha = ' + captchaTextValue);

				$.post('<%= testCaptchaURL.toString() %>', {
		        		captchaTextValue: captchaTextValue,
		        		gRecaptchaResponse: responseReCaptcha
	        		}).always(function(data){
	        			console.log("always func");
	               	var stringData = JSON.stringify(data.responseText) + "";
	               	eventoRespuesta.valido = (stringData.indexOf('true') !== -1);
	               	console.log('respuesta final captcha = ' + eventoRespuesta.valido) ;
				});
			}

		} else {
			var responseReCaptcha = grecaptcha ? grecaptcha.getResponse() : 'NA';
			console.log(responseReCaptcha);
			eventoRespuesta.valido = responseReCaptcha != "";
			eventoRespuesta.catpcha = responseReCaptcha;
			console.log("Respuesta de recaptcha -> eventoRespuesta  = " + eventoRespuesta);
			console.log(eventoRespuesta);
		}
		Liferay.fire('validaCaptchaRespuesta', eventoRespuesta );
	}
});
</aui:script>
