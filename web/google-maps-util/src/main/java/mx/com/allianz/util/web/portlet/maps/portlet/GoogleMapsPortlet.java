package mx.com.allianz.util.web.portlet.maps.portlet;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.css-class-wrapper=portlet-jsp",
		"com.liferay.portlet.display-category=Ionic",
		"com.liferay.portlet.header-portlet-css=/ionic/build/main.css",
		"com.liferay.portlet.footer-portlet-javascript=/ionic/build/polyfills.js",
		"com.liferay.portlet.footer-portlet-javascript=/ionic/build/main.js",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=Google Maps Web Util Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/ionic/index.jsp",
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user,guest"
	},
	service = Portlet.class
)
public class GoogleMapsPortlet extends MVCPortlet {
}